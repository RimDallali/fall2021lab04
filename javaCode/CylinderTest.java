//Rim Dallali ID : 1937259
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class CylinderTest {
    @Test
    public void testConstructorHeightException() {
        try {
            Cylinder cylinder = new Cylinder(0,5);
            fail("The test failed because of an invalid value for the height");
        }
        catch(IllegalArgumentException e) {

        }
    }

    @Test
    public void testConstructorRadiusException() {
        try {
            Cylinder cylinder = new Cylinder(5,0);
            fail("The test failed because of an invalid value for the radius");
        }
        catch(IllegalArgumentException e) {

        }
    }

    @Test
    public void testGetMethods() {
        Cylinder cylinder = new Cylinder(39,7);
        assertEquals (cylinder.getHeight(), 39);
        assertEquals (cylinder.getRadius(), 7);
    }

    @Test
    public void testSurfaceArea() {
        Cylinder cylinder = new Cylinder(23, 7);
        assertEquals(420*Math.PI, cylinder.getSurfaceArea());
    }
    
    @Test 
    public void testVolume() {
        Cylinder cylinder = new Cylinder(9, 2);
        assertEquals(36*Math.PI , cylinder.getVolume());
    }
}