// Raagav Prasanna 2031659
public class Cylinder {
    private double height;
    private double radius;

    public Cylinder(double height, double radius) {
       if(height <= 0 ) {
            throw new IllegalArgumentException("height must be greater than 0");
        }
        if(radius <= 0) {
            throw new IllegalArgumentException("radius must be greater than 0");
        }
        this.height = height;
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getHeight() {
        return(this.height);
    }

    public double getVolume() {
        return(Math.PI * Math.pow(this.radius, 2) *this.height);
    }

    public double getSurfaceArea() {
        return (2 * Math.PI * Math.pow(this.radius, 2)) + (2*Math.PI*this.radius*this.height);
    }
}