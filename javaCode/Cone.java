//Rim Dallali ID : 1937259
public class Cone {
    private double height;
    private double radius;

    public Cone(double height, double radius) {
        if(height <= 0) {
            throw new IllegalArgumentException("Invalid Height value");
        }
        
        if(radius <= 0) {
            throw new IllegalArgumentException("Invalid Radius value");
        }

        this.height = height;
        this.radius = radius;
    }

    public double getHeight() {
        return this.height;
    }
    
    public double getRadius() {
        return this.radius;
    }

    public double getVolume() {
        return Math.PI*this.radius*this.radius*(this.height/3);
    }
    
    public double getSurfaceArea() {
        return Math.PI*this.radius*(this.radius+ Math.sqrt((this.height*this.height)+(this.radius*this.radius)));
    }
}