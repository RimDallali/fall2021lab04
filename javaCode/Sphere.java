//Shay Alex Lelichev 2043812
public class Sphere {
    public double radius;

    public Sphere(double newRadius) {

        if (newRadius < 0) {
            throw new IllegalArgumentException("The radius of the sphere mush be a positive number!");
        } 
        else {
            this.radius = newRadius;
        }
    }

    public double getVolume(double radius) {
        return Math.PI * Math.pow(radius, 3);
    }

    public double getSurfaceArea(double radius) {
       return 4 * Math.PI * Math.pow(radius , 2);
    }

    public double getRadius() {
        return this.radius;
    }
}