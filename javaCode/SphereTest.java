// Raagav Prasanna 2036159
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class SphereTest {
    @Test
    public void testGetRadius() {
        Sphere s1 = new Sphere(5);

        assertEquals(5, s1.getRadius());
    }

    @Test
    public void testConstructorRadiusInput() {
        try {
            Sphere s1 = new Sphere(-1);
            fail("Test failed as IllegalArgumentException was not thrown");
        }
        catch(IllegalArgumentException e) {

        }
    }
    
    @Test
    public void testGetVolume() {
        Sphere s1 = new Sphere(2);
        assertEquals(Math.PI * 8, s1.getVolume(s1.getRadius()));
    }

    @Test
    public void getSurfaceArea() {
        Sphere s1 = new Sphere(3);
        assertEquals(Math.PI * 36, s1.getSurfaceArea(s1.getRadius()));
    }
}