//Shay Alex Lelichev 2043812

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class ConeTests {
    
    @Test
    public void testHeight() {
        try {
            Cone c = new Cone(0, 2);
            fail("The constructor succeeded");
        } catch (Exception e) {
        
        }
    }

    @Test
    public void testRadius() {
        try {
            Cone c = new Cone(2, 0);
            fail("The constructor succeeded");
        } catch (Exception e) {
        }
    }

    @Test
    public void testGetHeight() {
        Cone c = new Cone(6, 3);
        assertEquals(6, c.getHeight());
    }

    @Test
    public void testGetRadius() {
        Cone c = new Cone(3, 7);
        assertEquals(7, c.getRadius());
    }

    @Test
    public void testGetVolume() {
        Cone c = new Cone(6, 4);
        //assertEquals(Math.PI*4*4*(5/3), c.getVolume());
        assertEquals(Math.PI*4*4*(6/3), c.getVolume());
    }

    @Test
    public void testGetSurfaceArea() {
        Cone c = new Cone(9, 6);
        assertEquals(Math.PI*6*(6+ Math.sqrt((9*9)+(6*6))), c.getSurfaceArea());
    }
}
